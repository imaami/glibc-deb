commit c1f25758c353fcb16076512aba62746bff2e0d2d
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Fri Dec 13 03:23:14 2019 +0100

    hurd: Implement __close_nocancel_nostatus
    
    * sysdeps/mach/hurd/not-cancel.h: New file.

commit fe75ee0ca65536352fcdaebfbf39aac9bee271f0
Author: Andrew Eggenberger <andrew.eggenberger@gmail.com>
Date:   Tue Oct 29 23:19:32 2019 -0500

    hurd: add getrandom and getentropy implementations
    
    * sysdeps/mach/hurd/getentropy.c: New file.
    * sysdeps/mach/hurd/getrandom.c: Likewise.

commit 186e119bbd4a10895429ffe405ae96dc5c5634b8 (HEAD -> master, origin-rw/master)
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Fri Dec 13 03:32:21 2019 +0100

    hurd: Fix __close_nocancel_nostatus availability

    Not only libc/rtld use __close_nocancel_nostatus.

    * sysdeps/mach/hurd/Makefile [$(subdir) == io] (sysdep_routines): Add
    close_nocancel_nostatus.
    * sysdeps/mach/hurd/Versions (libc): Add __close_nocancel_nostatus to
    GLIBC_PRIVATE.
    * sysdeps/mach/hurd/not-cancel.h (__close_nocancel_nostatus): Declare
    function instead of defining inline.
    [IS_IN (libc) || IS_IN (rtld)] (__close_nocancel_nostatus): Make
    function hidden.
    * sysdeps/mach/hurd/close_nocancel_nostatus.c: New file.

commit 8eaf34eda256ba3647ed6e7ed5c7c9aa19955d17
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Fri Dec 13 10:10:59 2019 +0100

    hurd: Fix local PLT
    
    * include/sys/random.h (__getrandom): Add hidden prototype.
    * stdlib/getrandom.c (getrandom): Rename to hidden definition __getrandom.
    Add weak alias.
    * sysdeps/mach/hurd/getrandom.c (getrandom): Likewise.
    * sysdeps/unix/sysv/linux/getrandom.c (getrandom): Likewise.
    * sysdeps/mach/hurd/getentropy.c (getentropy): Use __getrandom instead of
    getrandom.

commit a45244ce127763872ff0b5743fb4ac8299ee9b28
Author: James Clarke <jrtc27@jrtc27.com>
Date:   Tue Dec 17 18:29:29 2019 +0000

    hurd: Make getrandom honour GRND_NONBLOCK
    
    * sysdeps/mach/hurd/getrandom.c (__getrandom): Open the random source
    with O_NONBLOCK when the GRND_NONBLOCK flag is provided.
    Message-Id: <20191217182929.90989-1-jrtc27@jrtc27.com>

---
 include/sys/random.h                        |   11 ++++
 stdlib/getrandom.c                          |    6 +-
 sysdeps/mach/hurd/Makefile                  |    2 
 sysdeps/mach/hurd/Versions                  |    1 
 sysdeps/mach/hurd/close_nocancel_nostatus.c |   30 +++++++++++++
 sysdeps/mach/hurd/getentropy.c              |   64 ++++++++++++++++++++++++++++
 sysdeps/mach/hurd/getrandom.c               |   50 +++++++++++++++++++++
 sysdeps/mach/hurd/not-cancel.h              |   62 +++++++++++++++++++++++++++
 sysdeps/unix/sysv/linux/getrandom.c         |    6 +-
 9 files changed, 227 insertions(+), 5 deletions(-)

--- /dev/null
+++ b/sysdeps/mach/hurd/not-cancel.h
@@ -0,0 +1,62 @@
+/* Uncancelable versions of cancelable interfaces.  Hurd version.
+   Copyright (C) 2003-2019 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+   Contributed by Ulrich Drepper <drepper@redhat.com>, 2003.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library; if not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#ifndef NOT_CANCEL_H
+# define NOT_CANCEL_H
+
+#include <fcntl.h>
+#include <unistd.h>
+#include <sys/wait.h>
+#include <time.h>
+#include <sys/uio.h>
+#include <hurd.h>
+#include <hurd/fd.h>
+
+/* For now we have none.  Map the name to the normal functions.  */
+#define __open_nocancel(...) \
+  __open (__VA_ARGS__)
+#define __open64_nocancel(...) \
+  __open64 (__VA_ARGS__)
+#define __openat_nocancel(...) \
+  __openat (__VA_ARGS__)
+#define __openat64_nocancel(...) \
+  __openat64 (__VA_ARGS__)
+#define __close_nocancel(fd) \
+  __close (fd)
+
+void __close_nocancel_nostatus (int fd);
+
+#define __read_nocancel(fd, buf, n) \
+  __read (fd, buf, n)
+#define __pread64_nocancel(fd, buf, count, offset) \
+  __pread64 (fd, buf, count, offset)
+#define __write_nocancel(fd, buf, n) \
+  __write (fd, buf, n)
+#define __writev_nocancel_nostatus(fd, iov, n) \
+  (void) __writev (fd, iov, n)
+# define __waitpid_nocancel(pid, stat_loc, options) \
+  __waitpid (pid, stat_loc, options)
+#define __fcntl64_nocancel(fd, cmd, ...) \
+  __fcntl64 (fd, cmd, __VA_ARGS__)
+
+#if IS_IN (libc) || IS_IN (rtld)
+hidden_proto (__close_nocancel_nostatus)
+#endif
+
+#endif /* NOT_CANCEL_H  */
--- /dev/null
+++ b/sysdeps/mach/hurd/getentropy.c
@@ -0,0 +1,64 @@
+/* Implementation of getentropy based on getrandom.
+   Copyright (C) 2016-2019 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library; if not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#include <sys/random.h>
+#include <assert.h>
+#include <errno.h>
+#include <unistd.h>
+
+/* Write LENGTH bytes of randomness starting at BUFFER.  Return 0 on
+   success and -1 on failure.  */
+int
+getentropy (void *buffer, size_t length)
+{
+  /* The interface is documented to return EIO for buffer lengths
+     longer than 256 bytes.  */
+  if (length > 256)
+    {
+      __set_errno (EIO);
+      return -1;
+    }
+
+  /* Try to fill the buffer completely.  Even with the 256 byte limit
+     above, we might still receive an EINTR error (when blocking
+     during boot).  */
+  void *end = buffer + length;
+  while (buffer < end)
+    {
+      /* NB: No cancellation point.  */
+      ssize_t bytes = __getrandom (buffer, end - buffer, 0);
+      if (bytes < 0)
+        {
+          if (errno == EINTR)
+            /* Try again if interrupted by a signal.  */
+            continue;
+          else
+            return -1;
+        }
+      if (bytes == 0)
+        {
+          /* No more bytes available.  This should not happen under
+             normal circumstances.  */
+          __set_errno (EIO);
+          return -1;
+        }
+      /* Try again in case of a short read.  */
+      buffer += bytes;
+    }
+  return 0;
+}
--- /dev/null
+++ b/sysdeps/mach/hurd/getrandom.c
@@ -0,0 +1,50 @@
+/* Hurdish implementation of getrandom
+   Copyright (C) 2016-2019 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library; if not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#include <sys/random.h>
+#include <fcntl.h>
+#include <unistd.h>
+#include <not-cancel.h>
+
+/* Write up to LENGTH bytes of randomness starting at BUFFER.
+   Return the number of bytes written, or -1 on error.  */
+ssize_t
+__getrandom (void *buffer, size_t length, unsigned int flags)
+{
+  const char *random_source = "/dev/urandom";
+  int open_flags = O_RDONLY | O_CLOEXEC;
+  size_t amount_read;
+  int fd;
+
+  if (flags & GRND_RANDOM)
+    random_source = "/dev/random";
+
+  if (flags & GRND_NONBLOCK)
+    open_flags |= O_NONBLOCK;
+
+  fd = __open_nocancel(random_source, open_flags);
+  if (fd == -1)
+    return -1;
+
+  amount_read = __read_nocancel(fd, buffer, length);
+  __close_nocancel_nostatus(fd);
+  return amount_read;
+}
+
+libc_hidden_def (__getrandom)
+weak_alias (__getrandom, getrandom)
--- a/sysdeps/mach/hurd/Makefile
+++ b/sysdeps/mach/hurd/Makefile
@@ -196,7 +196,7 @@ sysdep_routines += cthreads
 endif
 
 ifeq (io, $(subdir))
-sysdep_routines += f_setlk
+sysdep_routines += f_setlk close_nocancel_nostatus
 endif
 
 ifeq ($(subdir),sunrpc)
--- a/sysdeps/mach/hurd/Versions
+++ b/sysdeps/mach/hurd/Versions
@@ -13,6 +13,7 @@ libc {
     __libc_lock_self0;
 
     _dl_init_first;
+    __close_nocancel_nostatus;
   }
 }
 
--- /dev/null
+++ b/sysdeps/mach/hurd/close_nocancel_nostatus.c
@@ -0,0 +1,30 @@
+/* Copyright (C) 1991-2019 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library; if not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#include <errno.h>
+#include <unistd.h>
+#include <hurd.h>
+#include <hurd/fd.h>
+#include <not-cancel.h>
+
+/* Close the file descriptor FD.  */
+void
+__close_nocancel_nostatus (int fd)
+{
+  HURD_FD_USE (fd, _hurd_fd_close (descriptor));
+}
+libc_hidden_def (__close_nocancel_nostatus)
--- a/include/sys/random.h
+++ b/include/sys/random.h
@@ -1,1 +1,11 @@
+#ifndef _SYS_RANDOM_H
 #include <stdlib/sys/random.h>
+
+# ifndef _ISOMAC
+
+extern ssize_t __getrandom (void *__buffer, size_t __length,
+                            unsigned int __flags) __wur;
+libc_hidden_proto (__getrandom)
+
+# endif /* !_ISOMAC */
+#endif
--- a/stdlib/getrandom.c
+++ b/stdlib/getrandom.c
@@ -22,10 +22,12 @@
 /* Write up to LENGTH bytes of randomness starting at BUFFER.
    Return the number of bytes written, or -1 on error.  */
 ssize_t
-getrandom (void *buffer, size_t length, unsigned int flags)
+__getrandom (void *buffer, size_t length, unsigned int flags)
 {
   __set_errno (ENOSYS);
   return -1;
 }
-
 stub_warning (getrandom)
+
+libc_hidden_def (__getrandom)
+weak_alias (__getrandom, getrandom)
--- a/sysdeps/unix/sysv/linux/getrandom.c
+++ b/sysdeps/unix/sysv/linux/getrandom.c
@@ -25,7 +25,7 @@
 /* Write up to LENGTH bytes of randomness starting at BUFFER.
    Return the number of bytes written, or -1 on error.  */
 ssize_t
-getrandom (void *buffer, size_t length, unsigned int flags)
+__getrandom (void *buffer, size_t length, unsigned int flags)
 {
   return SYSCALL_CANCEL (getrandom, buffer, length, flags);
 }
@@ -33,7 +33,7 @@ getrandom (void *buffer, size_t length,
 /* Always provide a definition, even if the kernel headers lack the
    system call number. */
 ssize_t
-getrandom (void *buffer, size_t length, unsigned int flags)
+__getrandom (void *buffer, size_t length, unsigned int flags)
 {
   /* Ideally, we would add a cancellation point here, but we currently
      cannot do so inside libc.  */
@@ -41,3 +41,5 @@ getrandom (void *buffer, size_t length,
   return -1;
 }
 #endif
+libc_hidden_def (__getrandom)
+weak_alias (__getrandom, getrandom)
